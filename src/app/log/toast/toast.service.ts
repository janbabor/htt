import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Toast} from './types/toast';
import {first} from 'rxjs/operators';
import {ToastTypeEnum} from './types/toast-type.enum';

/**
 * This class represent a service which takes care of toast messages
 */
@Injectable({
  providedIn: 'root'
})
export class ToastService {

  private _timeout = 5000;
  private _toasts$ = new BehaviorSubject<Toast[]>([]);

  get toasts$(): Observable<Toast[]> {
    return this._toasts$.asObservable();
  }

  /**
   *
   * @param message The message which should be displayed in the toast.
   * @param heading A heading for the toast message.
   * @param type The message type (ToastTypeEnum)
   * @param timeout If provided toast will be visible for (timeout) time else will use default value.
   *
   * @return Toast Returns the appended toast.
   */
  add(message: string, heading: string, type: ToastTypeEnum, timeout = this._timeout): Toast {
    const toast: Toast = {message, heading, type, timeout};
    this.toasts$.pipe(first())
      .subscribe(toasts => this._toasts$.next([...toasts, toast]));
    return toast;
  }

  /**
   *
   * Removes a toast from the toast list.
   *
   * @param toast The toast which should be removed from the list
   */
  remove(toast: Toast): void {
    this.toasts$
      .pipe(first())
      .subscribe(toasts => this._toasts$.next(toasts.filter(t => t !== toast)));
  }

  /**
   *
   * Similar as {@link add} but uses predetermined values for type and timeout
   */
  success(message: string, heading: string = null): Toast {
    return this.add(message, heading, ToastTypeEnum.SUCCESS);
  }

  /**
   *
   * See {@link success}
   */
  info(message: string, heading: string = null): Toast {
    return this.add(message, heading, ToastTypeEnum.INFO);
  }

  /**
   *
   * See {@link success}
   */
  warning(message: string, heading: string = null): Toast {
    return this.add(message, heading, ToastTypeEnum.WARNING);
  }

  /**
   *
   * See {@link success}
   */
  danger(message: string, heading: string = null): Toast {
    return this.add(message, heading, ToastTypeEnum.DANGER);
  }
}
