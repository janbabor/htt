import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ToastService} from './toast.service';
import {Toast} from './types/toast';

/**
 * This class represent a toast component which displays toast from {@link ToastService}
 */
@Component({
  selector: 'htt-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToastComponent implements OnInit {
  dismissible = true;
  toasts$: Observable<Toast[]>;

  constructor(private toastService: ToastService) {
  }

  ngOnInit() {
    this.toasts$ = this.toastService.toasts$;
  }

  onClosed(toast: Toast): void {
    this.toastService.remove(toast);
  }
}
