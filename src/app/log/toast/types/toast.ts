import {ToastTypeEnum} from './toast-type.enum';

/**
 * A representation for a Toast object
 */
export interface Toast {
  type: ToastTypeEnum;
  message: string;
  heading: string;
  timeout: number;
}
