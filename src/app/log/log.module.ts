import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToastComponent} from './toast/toast.component';
import {AlertModule} from 'ngx-bootstrap';

/**
 * A class representation for the Log module which provides a toaster and a log service
 */
@NgModule({
  imports: [
    CommonModule,
    AlertModule
  ],
  declarations: [ToastComponent],
  exports: [ToastComponent]
})
export class LogModule {
}
