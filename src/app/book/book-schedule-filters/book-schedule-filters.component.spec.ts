import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookScheduleFiltersComponent } from './book-schedule-filters.component';

describe('BookScheduleFiltersComponent', () => {
  let component: BookScheduleFiltersComponent;
  let fixture: ComponentFixture<BookScheduleFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookScheduleFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookScheduleFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
