import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';

/**
 * Simple component displaying {@link BookScheduleComponent} form
 */
@Component({
  selector: 'htt-book-schedule-filters',
  templateUrl: './book-schedule-filters.component.html',
  styleUrls: ['./book-schedule-filters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookScheduleFiltersComponent {

  @Input() form: FormGroup;
}
