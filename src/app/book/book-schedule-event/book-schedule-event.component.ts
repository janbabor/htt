import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {ScheduleEvent} from '../types/schedule-event';
import * as moment from 'moment';
import {ToastService} from '../../log/toast/toast.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'htt-book-schedule-event',
  templateUrl: './book-schedule-event.component.html',
  styleUrls: ['./book-schedule-event.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookScheduleEventComponent {

  @Input() event: ScheduleEvent;
  @Input() selected: boolean;
  @Input() booked: boolean;

  @Output() select = new EventEmitter<ScheduleEvent>();

  get duration(): number {
    return moment(this.event.end, 'HH:mm:ss').diff(moment(this.event.start, 'HH:mm:ss'), 'minute');
  }

  constructor(private toastService: ToastService, private translateService: TranslateService) {
  }


  formatTimeString(): string {
    return moment(this.event.start, 'HH:mm:ss').format('HH:mm') + ' - ' +
      moment(this.event.end, 'HH:mm:ss').format('HH:mm');
  }

  onSelect(): void {
    if (this.event.possible && !this.booked) {
      this.select.emit(this.event);
    } else {
      this.toastService.warning(this.translateService.instant('TIME_EVENT_NOT_AVAILABLE', {time: this.formatTimeString()}));
    }
  }

}
