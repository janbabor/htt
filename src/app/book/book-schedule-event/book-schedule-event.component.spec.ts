import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookScheduleEventComponent } from './book-schedule-event.component';

describe('BookScheduleEventComponent', () => {
  let component: BookScheduleEventComponent;
  let fixture: ComponentFixture<BookScheduleEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookScheduleEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookScheduleEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
