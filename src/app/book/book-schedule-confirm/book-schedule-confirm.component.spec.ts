import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookScheduleConfirmComponent } from './book-schedule-confirm.component';

describe('BookScheduleConfirmComponent', () => {
  let component: BookScheduleConfirmComponent;
  let fixture: ComponentFixture<BookScheduleConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookScheduleConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookScheduleConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
