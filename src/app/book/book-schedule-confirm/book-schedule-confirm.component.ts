import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {ScheduleDay} from '../types/schedule-day';
import {ScheduleEvent} from '../types/schedule-event';
import * as moment from 'moment';
import {BookService} from '../book.service';
import {ToastService} from '../../log/toast/toast.service';
import {TranslateService} from '@ngx-translate/core';

/**
 * Component which display selected data, validates and requests a booking of a cleaner
 */
@Component({
  selector: 'htt-book-schedule-confirm',
  templateUrl: './book-schedule-confirm.component.html',
  styleUrls: ['./book-schedule-confirm.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookScheduleConfirmComponent {

  @Input() day: ScheduleDay;
  @Input() duration: number;
  @Input() postcode: number;

  @Output() booked = new EventEmitter<ScheduleDay>();

  get event(): string {
    return this.day ? this.getEventFormatString(this.day.startTimes[0]) : '';
  }

  get date(): string {
    return this.day ? moment(this.day.day).format('LL') : '';
  }

  constructor(private bookService: BookService, private toastService: ToastService, private translateService: TranslateService) {
  }

  private getEventFormatString(event: ScheduleEvent): string {
    return `${this.formatEventTime(event.start)} - ${this.formatEventTime(event.end)}`;
  }

  private formatEventTime(time: string): string {
    return moment(time, 'HH:mm:ss').format('LT');
  }

  request(): void {
    if (this.day && this.duration && this.postcode) {
      this.bookService.requestBooking({
        day: this.day.day,
        startTime: {
          start: this.day.startTimes[0].start,
          end: this.day.startTimes[0].end
        },
        visitDuration: this.duration,
        propertyId: this.postcode
      }).subscribe(response => {
          window.scrollTo(0, 0); // TODO remove ugly hack, Did this since I didn't have the time to fix a modal for the success message
          this.toastService.success(
            this.translateService.instant('BOOKING_SUCCESSFUL_TEXT', {
              cleaner: response.cleaner.name,
              date: moment(this.day.day).format('ll'),
              time: this.getEventFormatString(this.day.startTimes[0]),
              duration: this.duration
            }),
            this.translateService.instant('BOOKING_SUCCESSFUL')
          );
          this.booked.emit(this.day);
        }
      );
    } else {
      if (!this.day) {
        this.toastService.danger(this.translateService.instant('SELECTED_DAY_MISSING'));
      }
      if (!this.duration) {
        this.toastService.danger(this.translateService.instant('DURATION_MISSING'));
      }
      if (!this.postcode) {
        this.toastService.danger(this.translateService.instant('POSTCODE_MISSING'));
      }
    }
  }

}
