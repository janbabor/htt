import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as moment from 'moment';
import {Moment} from 'moment';
import {BehaviorSubject, Observable} from 'rxjs';
import {ScheduleDay} from './types/schedule-day';

/**
 * A service used to fetch and save schedule events
 */
@Injectable({
  providedIn: 'root'
})
export class BookService {

  private _bookApi = 'https://private-anon-596234df4a-housekeepavailability.apiary-mock.com';
  private _schedule = new BehaviorSubject<ScheduleDay[]>(null);

  get schedule$(): Observable<ScheduleDay[]> {
    return this._schedule.asObservable();
  }

  /**
   *
   * @param httpClient Injects {@link HttpClient} into the service
   */
  constructor(private httpClient: HttpClient) { // TODO inject book configuration to handle api destination
  }

  /**
   *
   * @param postcode The postcode to find schedule for.
   * @param date The date which week should be fetched
   * @param duration The duration of time which is preferred
   */
  getWeekSchedule(postcode: string, date: Moment = moment(), duration: number = 1) {
    this._schedule.next([]);
    this.httpClient.get(`${this._bookApi}/availability/?weekBeginning=${date.format('YYYY-MM-dd')}&visitDuration=${duration}&postcode=${postcode}`)
      .subscribe((response: ScheduleDay[]) => this._schedule.next(response));
  }

  /**
   *
   * @param params Object with request parameters for the booking
   */
  requestBooking(params: { day: string; startTime: { start: string; end: string }; visitDuration: number; propertyId: number }): Observable<any> {
    return this.httpClient.post(`${this._bookApi}/book/`, params);
  }
}
