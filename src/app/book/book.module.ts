import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BookRoutingModule} from './book-routing.module';
import {BookComponent} from './book.component';
import {LogModule} from '../log/log.module';
import {BookScheduleComponent} from './book-schedule/book-schedule.component';
import {BookScheduleDayComponent} from './book-schedule-day/book-schedule-day.component';
import {BookScheduleEventComponent} from './book-schedule-event/book-schedule-event.component';
import {TranslateModule} from '@ngx-translate/core';
import {BookScheduleConfirmComponent} from './book-schedule-confirm/book-schedule-confirm.component';
import {BookScheduleFiltersComponent} from './book-schedule-filters/book-schedule-filters.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BsDatepickerModule} from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BookRoutingModule,
    LogModule,
    TranslateModule,
    BsDatepickerModule
  ],
  declarations: [BookComponent, BookScheduleComponent, BookScheduleDayComponent, BookScheduleEventComponent, BookScheduleConfirmComponent, BookScheduleFiltersComponent]
})
export class BookModule {
}
