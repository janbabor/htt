import {ScheduleEvent} from './schedule-event';

export interface ScheduleDay {
  day: string;
  startTimes: ScheduleEvent[];

}
