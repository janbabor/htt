export interface ScheduleEvent {
  start: string;
  end: string;
  possible: boolean;
}
