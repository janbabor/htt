import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {ScheduleDay} from '../types/schedule-day';
import {ScheduleEvent} from '../types/schedule-event';
import * as moment from 'moment';

@Component({
  selector: 'htt-book-schedule-day',
  templateUrl: './book-schedule-day.component.html',
  styleUrls: ['./book-schedule-day.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookScheduleDayComponent {

  @Input() day: ScheduleDay;
  @Input() selectedEvent: ScheduleEvent;
  @Input() bookedEvents: ScheduleEvent[];

  @Output() select = new EventEmitter<ScheduleDay>();

  get weekDay(): string {
    return moment(this.day.day).format('dddd');
  }

  get formatDay(): string {
    return moment(this.day.day).format('LL');
  }

  selected(event: ScheduleEvent): boolean {
    return this.selectedEvent && this.isSameEvent(this.selectedEvent, event);
  }

  booked(event: ScheduleEvent): boolean {
    return this.bookedEvents.some(e => this.isSameEvent(e, event));
  }

  onSelect(event: ScheduleEvent): void {
    this.select.emit({
      day: this.day.day,
      startTimes: [event]
    });
  }

  isSameEvent(a: ScheduleEvent, b: ScheduleEvent): boolean {
    return a.start === b.start && a.end === b.end;
  }

}
