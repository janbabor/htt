import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookScheduleDayComponent } from './book-schedule-day.component';

describe('BookScheduleDayComponent', () => {
  let component: BookScheduleDayComponent;
  let fixture: ComponentFixture<BookScheduleDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookScheduleDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookScheduleDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
