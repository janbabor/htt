import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'htt-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
