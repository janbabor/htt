import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {BookService} from '../book.service';
import {Observable, Subject} from 'rxjs';
import {ScheduleDay} from '../types/schedule-day';
import {FormBuilder, FormGroup} from '@angular/forms';
import * as moment from 'moment';
import {debounceTime, filter, takeUntil, tap} from 'rxjs/operators';
import {ScheduleEvent} from '../types/schedule-event';

@Component({
  selector: 'htt-book-schedule',
  templateUrl: './book-schedule.component.html',
  styleUrls: ['./book-schedule.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookScheduleComponent implements OnInit, OnDestroy {

  schedule$: Observable<ScheduleDay[]>;

  selectedDay: ScheduleDay;
  bookedEvents: ScheduleDay[] = [];

  form: FormGroup;

  unsubscribe$ = new Subject<void>();

  constructor(private bookService: BookService, private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      date: null,
      duration: null,
      postcode: null
    });
  }

  ngOnInit() {
    this.schedule$ = this.bookService.schedule$;

    this.form.valueChanges
      .pipe(
        takeUntil(this.unsubscribe$),
        debounceTime(250),
        filter(values => values.date && values.duration && values.postcode),
        tap(() => this.selectedDay = null)
      )
      .subscribe(values => this.bookService.getWeekSchedule(values.postcode, moment(values.date), values.duration));

    this.form.setValue({date: moment().toDate(), duration: 2.5, postcode: 'EC1R 3BU'});
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onSelect(day: ScheduleDay): void {
    this.selectedDay = this.selectedDay && this.isSameDayAndEvent(day) ? null : day;
  }

  onBooked(day: ScheduleDay): void {
    if (this.selectedDay && this.isSameDayAndEvent(day)) {
      this.selectedDay = null;
    }
    this.bookedEvents.push(day);
  }

  getBookedEventsForDay(day: ScheduleDay): ScheduleEvent[] {
    return this.bookedEvents
      .filter(d => d.day === day.day)
      .reduce((a: ScheduleEvent[], b: ScheduleDay) => [...a, ...b.startTimes], []);
  }

  getSelectedEventByDay(day: ScheduleDay): ScheduleEvent {
    return this.selectedDay && this.selectedDay.day === day.day ? this.selectedDay.startTimes[0] : null;
  }

  isSameDayAndEvent(day: ScheduleDay): boolean {
    return this.selectedDay.day === day.day && this.selectedDay.startTimes[0].start === day.startTimes[0].start;
  }
}
