# HousekeepTechTest

This project was generated with [Angular CLI] version 6.2.2.
`ng new housekeep-tech-test -p htt --routing --style scss`

To install the application simply run `yarn` or `npm install` within the directory once the repository has been cloned to you computer. 

Frameworks, libraries and component packages being used for the test are;  
 *   Angular 6.1  
 *   MomentJS  
 *   Bootstrap (only styling)  
 *   Angular Bootstrap  
 *   ngx-translate  

So I think I made it a little bit more complicated in the beginning than I should had but it's always more fun to have some extra ambitions. 
The big loss is that I didn't have the time to write tests and document everything (ToastModule is best ducumented). The lack of documentation shouldn't be an issue since the code is pretty straight forward and I'm following Angular styleguide. 
Give or take I gave it about 4 hours, been running around fixing stuff at the same time since I just got back from a vacation. 
I've been using Webstorm for the code and been using Angular CLI for generating components, that's why all the spec files are there. 

If I had extra time;  
 *   I would implement a proper log service which I was in the beginning  
 *   I would write tests for all services and components.   
 *   I would fix the few TODOs I added to the code.  
 *   I would add async validation for UK postcodes.  
 *   Make it prettier  
 *   Added Prettier to the project.  
